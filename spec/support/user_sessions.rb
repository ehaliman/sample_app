module UserSessions
  def valid_signin(user)
    fill_in 'Email',    with: user.email
    fill_in 'Password', with: user.password
    click_button 'Sign in'

    # Sign in when not using Capybara as well.
    cookies[:remember_token] = user.remember_token
  end

  def fill_in_signup_form(name, email, password)
    fill_in "Name",                  with: name
    fill_in "Email",                 with: email
    fill_in "Password",              with: password
    fill_in "Confirm Password",      with: password
  end
end
